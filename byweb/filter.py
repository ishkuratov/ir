from importlib import reload

import byweb


__author__ = 'ilya'


reload(byweb.index.dictionary)
reload(byweb.index.filters)

index_dir = '/home/ilya/Documents/IR/Link to ByWebMergedIndex/'
output_dir = '/home/ilya/Documents/IR/Link to ByWebFilteredIndices/'
# byweb.index.filters.filter_numbers(index_dir, output_dir)

# byweb.index.filters.exclude_unfolded(output_dir, output_dir)

# byweb.index.filters.merge_with_folded(output_dir, output_dir)

# byweb.index.filters.filter_stop_words(output_dir, output_dir)

# byweb.index.filters.filter_non_russian(output_dir, output_dir)

byweb.index.filters.lemmatisation(output_dir, output_dir)