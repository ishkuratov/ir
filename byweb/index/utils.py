from os import listdir, path

__author__ = 'ilya'


def get_dump_files(source):
    dumps = [path.join(source, dump) for dump in listdir(source) if dump.endswith('.dump')]
    return dumps


def get_dump_start_with(source, start_with):
    dumps = [path.join(source, dump) for dump in listdir(source) if
             dump.startswith(start_with) and dump.endswith('.dump')]
    return dumps