import json
import re
from os import path
import unicodedata

from nltk.corpus import stopwords
import pymorphy2

from . import utils, dictionary


__author__ = 'ilya'


def filter_numbers(index_dir='.', output_dir='.', group_by=20):
    parts = sorted(utils.get_dump_files(index_dir))

    posting_len = 0
    dict_size = 0
    part_number = 1
    current_number = 0
    parts_total = len(parts)
    refined_index = dictionary.Dictionary()
    for part in parts:
        current_number += 1
        print('Processing part {0:s}'.format(path.basename(part)))
        dict_dump = dictionary.DictionaryDump([part])
        for (term, posting) in dict_dump:
            m = re.match('\w*\d.*', term)
            if not m:
                refined_index.add_entry(term, posting)
                posting_len += len(posting)

        if (current_number % group_by == 0) or (current_number == parts_total):
            dict_size += len(refined_index)
            print('Save dictionary')
            print('Dictionary size {0:d}'.format(dict_size))
            filename = path.join(output_dir, 'no_number_index_{0:04d}.dump'.format(part_number))
            refined_index.save(filename)
            refined_index = dictionary.Dictionary()
            part_number += 1

    filename = path.join(output_dir, 'no_number_stats.json')
    with open(filename, 'w+') as f:
        d = dict()
        d['Dictionary size'] = dict_size
        d['Posting total length'] = posting_len
        json.dump(d, f, indent=4)


def exclude_unfolded(index_dir='.', output_dir='.'):
    parts = sorted(utils.get_dump_start_with(index_dir, 'no_number'))

    part_number = 0
    folded_index = dictionary.Dictionary()
    excluded_index = dictionary.Dictionary()
    for part in parts:
        part_number += 1
        print('Processing part {0:s}'.format(path.basename(part)))
        dict_dump = dictionary.DictionaryDump([part])
        for (term, posting) in dict_dump:
            folded = term.casefold()
            if folded == term:
                folded_index.add_entry(term, posting)
            else:
                excluded_index.add_entry(folded, posting)
        print('Excluded terms number {0:d}'.format(len(excluded_index)))
        print('Folded terms number {0:d}'.format(len(folded_index)))
        folded_index.save(path.join(output_dir, 'folded_index_{0:04d}.dump'.format(part_number)))
        folded_index = dictionary.Dictionary()
    excluded_index.save(path.join(output_dir, 'excluded_index.dump'))


def merge_with_folded(index_dir='.', output_dir='.', post_list_bound=15000000):
    folded_dumps = sorted(utils.get_dump_start_with(index_dir, 'folded'))
    excluded_dump = utils.get_dump_start_with(index_dir, 'excluded')
    if len(excluded_dump) > 1:
        raise AssertionError('There must be single index with excluded terms.')

    aggregator = dictionary.Aggregator(output_dir, basename='merged_folded', post_list_bound=post_list_bound)
    index_iters = [iter(dictionary.DictionaryDump(excluded_dump)), iter(dictionary.DictionaryDump(folded_dumps))]
    aggregator.merge_with_iters(index_iters, stat_filename='merge_folded_stats')


def filter_stop_words(index_dir='.', output_dir='.'):
    folded_dump = utils.get_dump_start_with(index_dir, 'merged_folded')
    if len(folded_dump) > 1:
        raise AssertionError('There must be single index with excluded terms.')

    filtered_index = dictionary.Dictionary()

    stop_words = set(stopwords.words('russian'))
    folded_index = dictionary.DictionaryDump(folded_dump)
    for (term, posting) in folded_index:
        if term not in stop_words:
            filtered_index.add_entry(term, posting)

    filtered_index.save(path.join(output_dir, 'no_stop_words_index.dump'))
    filtered_index.save_size(path.join(output_dir, 'no_stop_words_stats.json'))


def filter_non_russian(index_dir='.', output_dir='.'):
    folded_dump = utils.get_dump_start_with(index_dir, 'no_stop_words')
    if len(folded_dump) > 1:
        raise AssertionError('There must be single index file without stop words.')

    cyrillic_letters= {}

    def is_cyrillic(uchr):
        try: return cyrillic_letters[uchr]
        except KeyError:
             return cyrillic_letters.setdefault(uchr, unicodedata.name(uchr).startswith('CYRILLIC'))

    def only_cyrillic_chars(unistr):
        return all(is_cyrillic(uchr) for uchr in unistr if uchr.isalpha())

    filtered_index = dictionary.Dictionary()
    folded_index = dictionary.DictionaryDump(folded_dump)
    for (term, posting) in folded_index:
        if only_cyrillic_chars(term):
            filtered_index.add_entry(term, posting)

    filtered_index.save(path.join(output_dir, 'only_cyrillic_index.dump'))
    filtered_index.save_size(path.join(output_dir, 'only_cyrillic_stats.json'))


def lemmatisation(index_dir='.', output_dir='.'):
    folded_dump = utils.get_dump_start_with(index_dir, 'only_cyrillic')
    if len(folded_dump) > 1:
        raise AssertionError('There must be single index file with cyrillic words.')

    analyzer = pymorphy2.MorphAnalyzer()
    filtered_index = dictionary.Dictionary()
    folded_index = dictionary.DictionaryDump(folded_dump)
    for (term, posting) in folded_index:
        normalized_term = analyzer.parse(term)[0].normal_form
        filtered_index.add_entry(normalized_term, posting)

    filtered_index.save(path.join(output_dir, 'lemmatized_index.dump'))
    filtered_index.save_size(path.join(output_dir, 'lemmatized_stats.json'))

