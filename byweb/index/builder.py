import threading
import gc

from byweb.index import document
from byweb.index import collection


__author__ = 'ilya'


def build(src):
    """
    Build collection from specified file.
    :param src: collection file
    :return: collection of documents
    """
    coll = collection.Collection()
    with open(src, 'r') as f:
        for line in f:
            doc = document.Document(line)
            coll.add_document(doc)
    return coll


# TODO: Replace threads with processes.
def par_build(src, thread_num=2, storage='.', base_name='collection_part'):
    import concurrent.futures
    import pickle
    from os import listdir, path

    # Get all parts of the collection.
    parts = [file for file in listdir(src) if file.endswith('.xml.txt')]
    parts_num = len(parts)

    chunks_num = min(thread_num * 32, parts_num)
    print('Chunks number is %d' % chunks_num)
    # Distribute parts between threads.
    chunks = [[] for i in range(chunks_num)]
    for i in range(parts_num):
        abs_path = path.abspath(path.join(src, parts[i]))
        chunks[i % chunks_num].append(abs_path)

    chunk_names = []
    for i in range(chunks_num):
        chunk_names.append('%s_%04d.dump' % (base_name, i))

    chunks_dict = dict(zip(chunk_names, chunks))

    finished_count = 0
    with concurrent.futures.ThreadPoolExecutor(max_workers=thread_num) as executor:
        build_collection = {executor.submit(_par_add_to_coll, chunk, name): name for name, chunk in chunks_dict.items()}
        for future in concurrent.futures.as_completed(build_collection):
            name = build_collection[future]
            coll = future.result()
            abs_path = path.abspath(path.join(storage, name))
            with open(abs_path, 'wb+') as f:
                pickle.dump(coll, f, pickle.HIGHEST_PROTOCOL)
            # Clear result to free memory.
            future._result = None

            finished_count += 1
            print('{0:d}/{1:d} finished, last collection size is {2:d}'.format(finished_count, chunks_num, coll.get_size()))
            coll = None
            unreachable = gc.collect()
            print('%d unreachable objects.\n' % unreachable)


def _par_add_to_coll(files, chunk_name):
    th_name = threading.current_thread().getName()
    print('{0:s} start chunk: {1:s}\n'.format(th_name, chunk_name))
    coll = collection.Collection()
    for file in files:
        with open(file, 'r') as f:
            for line in f:
                doc = document.Document(line)
                coll.add_document(doc)
    print('{0:s} finish chunk {1:s}\n'.format(th_name, chunk_name))
    return coll