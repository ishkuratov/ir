import pickle

from byweb.index import utils


class Graph:
    def __init__(self):
        self._vertices = dict()

    def keys(self):
        return self._vertices.keys()

    def values(self):
        return self._vertices.values()

    def __len__(self):
        return len(self._vertices)

    def __getitem__(self, item):
        return self._vertices[item]

    def add_entry(self, url, references):
        if url in self._vertices:
            self._vertices[url].update(references)
        else:
            self._vertices[url] = Graph.References(references)

    def count_reference(self):
        for (url, references) in self._vertices.items():
            print(url)
            for reference in references:
                if reference in self._vertices:
                    self._vertices[reference].add_inlink()

    def ref_count_generator(self):
        return ((url, self._vertices[url].get_inlink_count()) for url in
                sorted(self._vertices, key=lambda x: self._vertices[x].get_inlink_count(), reverse=True))

    class References:
        def __init__(self, references: set()):
            self._references = references
            self._inlink_count = 0

        def __iter__(self):
            return iter(self._references)

        def get_inlink_count(self):
            return self._inlink_count

        def update(self, references: set()):
            self._references.update(references)

        def add_inlink(self):
            self._inlink_count += 1


def build_from_collection(collection_dir):
    coll_dumps = utils.get_dump_files(collection_dir)

    reference_graph = Graph()
    for coll_dump in coll_dumps:
        print(coll_dump)
        with open(coll_dump, 'rb') as f:
            coll_part = pickle.load(f)
            for document in coll_part.documents:
                reference_graph.add_entry(document.url, document.references)

    return reference_graph