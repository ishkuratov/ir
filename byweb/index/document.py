import re
import nltk

__author__ = 'ilya'


class Document:

    def __init__(self, line):
        self.text = [line]
        # self.id, end = self._get_id(line)
        # self.url, end = self._get_url(line, end)
        self.id, self.url, end = self._get_id_url(line)
        self.text, self.references = self._filter_markup(line, end)
        # self.references = self._search_references(line)
        self.size_raw = self._get_length_in_bytes()
        self._tokenize()
        self.size_tokenized = self._get_length_in_bytes()
        self.length_in_words = len(self.text)
        self.length_in_symbols = self._get_length_in_symbols()


    @staticmethod
    def _get_id(line) -> (int, int):
        m = re.match('.*?(\d+)\s+', line)
        if not m:
            print('Can''t extract id: %s' % line)
            return -1, 0
        id = int(m.group(1))
        end = m.end()
        return id, end

    @staticmethod
    def _get_url(line, start) -> (str, int):
        """
    
        :param line: document line
        :param start: start position for url retrieval
        :return:
        """
        m = re.match('\s*http://(.*?).by/.*?\s+', line[start:])
        if not m:
            print('Can''t retrieve domain address')
            return '', start
        domain = m.group(1)
        end = m.end() + start
        return domain, end

    @staticmethod
    def _get_id_url(line):
        m = re.match('.*?(\d+)\s+http://(.*?).by/.*?\s+', line)
        if not m:
            print('Can''t extract id or url: %s' % line)
            return -1, '', 0
        doc_id = int(m.group(1))
        url = m.group(2)
        end = m.end()
        return doc_id, url, end

    def _filter_markup(self, line, start) -> (list, list):
        """

        :param start:
        :return:
        """
        block = '(<.*?>)'
        # tokens = re.split(block, line[start:])
        tokens = re.split(block, line)
        href = r'''<([aA] +href=['"]http://(.+?)\.by/.*?['"].*?)?.*?>'''
        text, references = [], set()
        for token in tokens:
            m = re.match(href, token)
            if m:
                (bracket, domain) = m.groups()
                if domain and domain != self.url:
                    # print(domain)
                    references.add(domain)
            else:
                text.append(token)
        return text, references

    def _search_references(self, line) -> set:
        href = r'''<[aA] +href=['"]http://(.+?)\.by/.*?['"].*?>'''
        references = set(re.findall(href, line))
        references.discard(line)
        return references

    def _get_length_in_bytes(self) -> int:
        size = 0
        for chunk in self.text:
            size += len(chunk.encode())
        return size

    def _get_length_in_symbols(self) -> int:
        size = 0
        for token in self.text:
            size += len(token)
        return size

    def _tokenize(self):
        tokens = []
        tokenizer = nltk.tokenize.RegexpTokenizer('(\w+)|(\d+)')
        for chunk in self.text:
            chunks = tokenizer.tokenize(chunk)
            tokens.extend(chunks)
        self.text = tokens