__author__ = 'ilya'


class PostingsList:
    """
    Represents index postings list.
    """

    def __init__(self):
        self._postings = set()
        self._collection_frequency = 0

    def __len__(self):
        return len(self._postings)

    @property
    def coll_freq(self):
        return self._collection_frequency

    def add_entry(self, doc_id, count):
        self._collection_frequency += count
        self._postings.add(doc_id)

    def merge_with(self, postings_list) -> int:
        old_size = len(self._postings)
        self._postings.union(postings_list._postings)
        # TODO: fix collection frequency name.
        self._collection_frequency += postings_list._collection_frequency
        return len(self._postings) - old_size
