import json

__author__ = 'ilya'


class Collection:
    def __init__(self):
        self.documents = []

    def add_document(self, doc):
        self.documents.append(doc)

    def get_size(self):
        return len(self.documents)


class CollectionStat:
    def __init__(self):
        # Total statistics.
        self.docs_raw_size = 0
        self.docs_clear_size = 0
        self.docs_size_words = 0
        self.docs_size_symbols = 0
        self.docs_number = 0

        # Average statistics.
        self.docs_avg_size_raw = 0
        self.docs_avg_size_clear = 0
        self.docs_avg_size_words = 0
        self.words_avg_len = 0

    def add_document(self, doc):
        if doc.size_tokenized == 0:
            return
        self.docs_raw_size += doc.size_raw
        self.docs_clear_size += doc.size_tokenized
        self.docs_size_words += doc.length_in_words
        self.docs_size_symbols += doc.length_in_symbols
        self.docs_number += 1

    def get_tokens_number(self):
        return self.docs_size_words

    def get_avg_stats(self):
        self.docs_avg_size_raw = self.docs_raw_size / self.docs_number
        self.docs_avg_size_clear = self.docs_clear_size / self.docs_number
        self.docs_avg_size_words = self.docs_size_words / self.docs_number
        self.words_avg_len = self.docs_size_symbols / self.docs_size_words
        return self.docs_avg_size_raw, self.docs_avg_size_clear, self.docs_avg_size_words, self.words_avg_len

    def to_dict(self):
        d = dict()
        self.get_avg_stats()
        d['docs_raw_size'] = self.docs_raw_size
        d['docs_clear_size'] = self.docs_clear_size
        d['docs_size_words'] = self.docs_size_words
        d['docs_size_symbols'] = self.docs_size_symbols
        d['docs_number'] = self.docs_number
        d['docs_avg_size_raw'] = self.docs_avg_size_raw
        d['self.docs_avg_size_clear'] = self.docs_avg_size_clear
        d['docs_avg_size_words'] = self.docs_avg_size_words
        d['words_avg_len'] = self.words_avg_len
        return d

    def save(self, filename):
        with open(filename, 'w+') as f:
            json.dump(self.to_dict(), f, indent=4)