import collections
import heapq
import json
import pickle
from os import path

from . import posting, collection, utils


class Dictionary:
    def __init__(self):
        self._dictionary = dict()
        self._dict_stats = []
        self._terms_added = 0

    def __len__(self):
        return len(self._dictionary)

    def __getitem__(self, item):
        return self._dictionary[item]

    def keys(self):
        return self._dictionary.keys()

    def values(self):
        return self._dictionary.values()

    def postings(self):
        return self._dictionary.values()

    def stats(self):
        return self._dict_stats

    def add_document(self, document):
        counter = collections.Counter(document.text)
        for term, count in counter.items():
            self._terms_added += count
            if term in self._dictionary:
                self._dictionary[term].add_entry(document.id, count)
            else:
                self._dictionary[term] = posting.PostingsList().add_entry(document.id, count)
        self._dict_stats.append((len(self._dictionary), self._terms_added))

    def add_entry(self, term, posting_list) -> int:
        """
        Add new entry to index ot merge with existing one.
        :param term: term to add
        :param posting_list: posting list for term
        :return: postings increment.
        """
        if term in self._dictionary:
            diff = self._dictionary[term].merge_with(posting_list)
        else:
            self._dictionary[term] = posting_list
            diff = len(posting_list)
        return diff

    def add_dictionary_dump(self, dict_dump):
        """

        :param dict_dump: dictionary, dumped to file as set of tuples (term, posting)
        :type dict_dump: DictionaryDump
        :return:
        """
        for (term, posting_list) in dict_dump:
            self.add_entry(term, posting_list)

    def save(self, filename):
        with open(filename, 'wb+') as f:
            pickle.dump(self._dict_stats, f)
            pickle.dump(len(self._dictionary), f)
            s_term = sorted(self._dictionary)
            for term in s_term:
                pickle.dump((term, self._dictionary[term]), f)

    def save_size(self, filename):
        post_list_total_len = 0
        for post in self._dictionary.values():
            post_list_total_len += len(post)
        with open(filename, 'w+') as f:
            d = {'Dictionary size': len(self._dictionary), 'Posting total length': post_list_total_len}
            json.dump(d, f, indent=4)


class DictionaryDump:
    """
    Iterable abstraction of dumped dictionary.
    """
    def __init__(self, dump_files):
        """

        :param dump_files: list of dump files that are parts of single index.
        :type dump_files: list[str]
        :return:
        """
        if not dump_files:
            raise ValueError('Dump file list must be non-empty!')
        self._dump_files = dump_files
        self._dump = None
        self._entries_number = None
        self._current_position = None
        self._next_dump()

    def _next_dump(self):
        try:
            dump_file = self._dump_files.pop(0)
            self._dump = open(dump_file, 'rb')
            pickle.load(self._dump)  # Skip dictionary statistic.
            self._entries_number = pickle.load(self._dump)
            self._current_position = 0
        except IndexError:
            self._dump = None
            self._entries_number = 0
            self._current_position = 0

    def __len__(self):
        return self._entries_number

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def next(self) -> (str, posting.PostingsList):
        if self._current_position < self._entries_number:
            self._current_position += 1
            return pickle.load(self._dump)
        else:
            # Switch to next dump file.
            self._dump.close()
            self._next_dump()
            if self._dump:
                return self.next()
            else:
                raise StopIteration()

    def is_next(self):
        return self._current_position < self._entries_number

    def close(self):
        self._dump.close()
        self._current_position = -1
        self._entries_number = 0


class Builder:
    def __init__(self, output_dir='.', basename='index_part', files_per_part=4):
        self._output_dir = output_dir
        self._basename = basename
        self._files_per_part = files_per_part

    def build_parts(self, collection_dir):
        parts = utils.get_dump_files(collection_dir)
        parts_number = len(parts)
        curr_part_number = 0
        dict_part_number = 1

        coll_stat = collection.CollectionStat()
        dict_part = Dictionary()
        for part in parts:
            with open(part, 'rb') as p:
                coll = pickle.load(p)
                for document in coll.documents:
                    coll_stat.add_document(document)
                    dict_part.add_document(document)

            curr_part_number += 1
            print('{0:d} parts of {1:d}'.format(curr_part_number, parts_number))
            print('Dictionary size is {0:d}'.format(len(dict_part)))
            if curr_part_number % self._files_per_part == 0:
                print('Save index part: {0:d}'.format(dict_part_number))
                dict_part.save(self._get_filename(dict_part_number))
                dict_part_number += 1
                dict_part = Dictionary()
                print('Saved')

        if dict_part:
            dict_part.save(self._get_filename(dict_part_number))
        coll_stat.save(path.join(self._output_dir, 'statistic.json'))

    def _get_filename(self, part_id):
        filename = '{0:s}_{1:04d}.dump'.format(self._basename, part_id)
        return path.join(self._output_dir, filename)


class Aggregator:
    def __init__(self, output_dir='.', basename='merged_index_part', post_list_bound=15000000):
        self._output_dir = output_dir
        self._basename = path.join(self._output_dir, basename)
        self._post_list_bound = post_list_bound

        self._registry = self.TermRegistry()  # {term: count}
        self._part_number = 1
        self._post_list_curr_len = 0

        # Statistics.
        self._dict_size = 0
        self._post_list_total_len = 0

    def merge(self, index_dir):
        parts = utils.get_dump_files(index_dir)
        if len(parts) < 2:
            raise AssertionError('There must be at least two index files in directory.')

        dict_iters = list()
        for part in parts:
            dict_dump = DictionaryDump([part])
            dict_iters.append(iter(dict_dump))

        self.merge_with_iters(dict_iters)

    def merge_with_iters(self, dict_iters, stat_filename='statistics'):
        # Initialize terms queue.
        pr_queue = list()  # [(term, term_id, dict_iter, postings_list)]
        for dict_iter in dict_iters:
            (term, post_list) = dict_iter.next()
            term_id = self._registry.add_term(term)
            pr_queue.append((term, term_id, dict_iter, post_list))

        merged_index = Dictionary()  # {term: posting}
        heapq.heapify(pr_queue)
        # Extract next term entry from queue.
        (next_term, _, dict_iter, next_post_list) = heapq.heappop(pr_queue)
        while pr_queue:
            removed = self._registry.remove_term(next_term)

            # Add entry to general index.
            self._post_list_curr_len += merged_index.add_entry(next_term, next_post_list)
            if removed and (self._post_list_curr_len > self._post_list_bound):
                print('Chunk size: {0:d}'.format(len(merged_index)))
                self._save_part(merged_index)
                merged_index = Dictionary()
                self._post_list_curr_len = 0

            try:
                # Extract next term entry from dumped index.
                (term, post_list) = dict_iter.next()
                term_id = self._registry.add_term(term)
                new_entry = (term, term_id, dict_iter, post_list)
                # Extract next term entry from queue.
                (next_term, _, dict_iter, next_post_list) = heapq.heappushpop(pr_queue, new_entry)
            except StopIteration:
                # Extract next term entry from queue.
                (next_term, _, dict_iter, next_post_list) = heapq.heappop(pr_queue)
                continue

        if merged_index:
            self._save_part(merged_index)
        print('Dictionary size: {0:d}'.format(self._dict_size))
        print('Postings total length: {0:d}'.format(self._post_list_total_len))
        self.save_statistics(path.join(self._output_dir, '{0:s}.json'.format(stat_filename)))

    def _save_part(self, merged_index: Dictionary):
        print('Save part {0:d}'.format(self._part_number))
        merged_index.save('{0:s}_{1:04d}.dump'.format(self._basename, self._part_number))

        self._dict_size += len(merged_index)
        print('Dictionary size: {0:d}'.format(self._dict_size))

        self._post_list_total_len += self._post_list_curr_len
        print('Posting current length: {0:d}'.format(self._post_list_total_len))

        self._part_number += 1

    def save_statistics(self, filename):
        d = dict()
        d['Dictionary size'] = self._dict_size
        d['Postings total length'] = self._post_list_total_len
        with open(filename, 'w+') as f:
            json.dump(d, f, indent=4)

    class TermRegistry:
        def __init__(self):
            self.registry = dict()

        def add_term(self, term):
            if term in self.registry:
                self.registry[term] += 1
            else:
                self.registry[term] = 1
            return self.registry[term]

        def remove_term(self, term) -> bool:
            if self.registry[term] == 1:
                del self.registry[term]
                return True
            else:
                self.registry[term] -= 1
                return False

        def get_term_count(self, term):
            return self.registry[term]