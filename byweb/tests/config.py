import os

__author__ = 'ilya'


module_dir = os.path.dirname(os.path.realpath(__file__))
resources_dir = os.path.join(module_dir, 'resources')
results_dir = os.path.join(module_dir, 'results')
