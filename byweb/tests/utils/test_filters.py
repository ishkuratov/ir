import os
from ...tests.config import resources_dir, results_dir

__author__ = 'ilya'


def markup_filter_test():
    from ...utils import filters

    test_file = os.path.join(resources_dir, 'line.txt')
    result_file = os.path.join(results_dir, 'markup_filter.txt')
    print(test_file)
    with open(test_file, 'r') as f:
        line = f.readline()
        (text, urls) = filters.markup_filter(line)
        with open(result_file, 'w+') as result:
            result.writelines(text)
            for url in urls:
                result.write('%s\n' % url)
    return text, urls


def url_filter_test(urls):
    from ...utils.filters import url_filter

    urls = url_filter(urls, 'www.source.by')

    assert len(urls) == 2
    assert 'wwp.test.by' in urls
    assert 'wwp.test2.by' in urls

if __name__ == '__main__':
    text, urls = markup_filter_test()
    url_filter_test(urls)