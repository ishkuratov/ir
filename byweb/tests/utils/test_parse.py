import os
from ...tests.config import resources_dir


def get_id_test(line):
    from ...utils.parse import get_id

    id, line, end = get_id(line)
    print(id)
    assert id == 4
    return line, end


def get_domain_test(line, start):
    from ...utils.parse import get_domain

    domain, line, end = get_domain(line, start)
    print(domain)
    assert domain == 'forum.linux.by'


if __name__ == '__main__':
    test_file = os.path.join(resources_dir, 'line.txt')
    with open(test_file, 'r') as f:
        line = f.readline()
        line, end = get_id_test(line)
        get_domain_test(line, end)