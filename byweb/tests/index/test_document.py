import os
from unittest import TestCase
import unittest
from byweb.index.document import Document
from byweb.tests.config import resources_dir

__author__ = 'ilya'

class TestDocument(TestCase):

    def setUp(self):
        test_file = os.path.join(resources_dir, 'line.txt')
        with open(test_file, 'r') as f:
            self.line = f.readline()

    def test__get_id(self):
        print('test1')
        id, end = Document._get_id(self.line)
        self.assertEqual(4, id)

    def test__get_url(self):
        print('test2')
        url, end = Document._get_url(self.line, 2)
        self.assertEqual('forum.linux.by', url)

    def test__filter_markup(self):
        print('test3')
        text, references = Document._filter_markup(self.line, 77)
        self.assertEqual(12, len(text))
        self.assertEqual(4, len(references))


if __name__ == '__main__':
    unittest.main()
