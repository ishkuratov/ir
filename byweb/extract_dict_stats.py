import pickle

import byweb


__author__ = 'ilya'


index_dir = '/home/ilya/Documents/IR/Link to ByWebIndex/'

parts = byweb.index.utils.get_dump_files(index_dir)

stats = list()
for part in parts:
    with open(part, 'b') as f:
        index_part = pickle.load(f)
        stats.append(index_part.stats())


