import re

__author__ = 'ilya'


def markup_filter(line):
    block = '(<.*?>)'
    tokens = re.split(block, line)

    href = r'''<([aA] +href=['"]http://(.+?)/.*?['"].*?)?.*?>'''
    text, external_urls = [], []
    for token in tokens:
        m = re.match(href, token)
        if m:
            (bracket, domain) = m.groups()
            if domain:
                print(domain)
                external_urls.append(domain)
            else:
                print('delete')
        else:
            text.append(token)

    return text, external_urls


def url_filter(urls, source):
    return {url for url in urls if url.endswith('.by') and url != source}