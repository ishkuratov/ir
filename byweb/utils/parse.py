import re


def get_id(line):
    m = re.match('\d+\s+', line)
    if not m:
        return -1, line, 0
    id = int(m.group())
    end = m.end()
    return id, line, end


def get_domain(line, start):
    m = re.match('\s*http://(.*?)/.*?\s+', line[start:])
    if not m:
        return '', line, start
    domain = m.group(1)
    end = m.end() + start
    return domain, line, end